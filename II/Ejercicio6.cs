using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio6
{
    class Ejercicio6
    {
        public static double calcularFactorial(double numero)
        {
            for(double i = numero - 1; i > 0; i--)
            {
                numero *= i; 
            }
            return numero;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Introduce un numero para calcular su factorial: ");
            double numero = double.Parse(Console.ReadLine());
            Console.WriteLine("El factorial de {0} es {1}", numero, calcularFactorial(numero));
            Console.ReadKey();
        }
    }
}