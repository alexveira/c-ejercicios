using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio10
{


    class Ejercicio10
    {
        static int potencia(int numero, int exponente)
        {
            int resultado = 1;
            for (int i = 0; i < exponente; i++)
            {
                resultado *= numero;
            }
            return resultado;
        }
        static void Main(string[] args)
        {
            Console.WriteLine(potencia(13, 3));
            Console.ReadKey();

        }
    }
}
