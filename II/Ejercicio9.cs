using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio9
{

    class Ejercicio9
    {
        static void printStringInPosition(string texto, int x, int y)
        {
            Console.SetCursorPosition(x, y);
            Console.WriteLine(texto);

        }

        static void Main(string[] args)
        {
            Console.WriteLine("Introduce un texto");
            string texto = Console.ReadLine();
            Console.WriteLine("Introduce una posicion x:");
            int x = int.Parse(Console.ReadLine());
            Console.WriteLine("Introduce una posicion y:");
            int y = int.Parse(Console.ReadLine());
            printStringInPosition(texto, x, y);
            Console.ReadKey();

        }
    }
}
