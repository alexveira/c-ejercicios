using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace Ejercicio1
{
    class Ejercicio1
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Nombre del fichero:");
            string namefyle = Console.ReadLine();
            StreamReader sr;
            sr = File.OpenText(namefyle);
            Console.WriteLine("Letra a buscar: ");
            string letter = Console.ReadLine();
            string line;
            int count = 0;
            do
            {
                line = sr.ReadLine();
                if (line != null)
                    for (int i = 0; i < line.Length; i++)
                    {
                        if (line.Substring(i,1) == letter)
                            count++;
                    }
                
            }while(line != null);
            sr.Close();
            Console.WriteLine("El carácter {0} se repite {1} veces dentro del fichero: {2}", letter, count, namefyle);
            Console.ReadKey();
        }
    }
}
