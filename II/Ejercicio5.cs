using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio5
{
    class Ejercicio5
    {

        static string encriptar(string noencriptado)
        {
            char[] chararray = noencriptado.ToCharArray();
            int[] encriptedint = new int[chararray.Length];
            for (int i = 0; i < chararray.Length; i++)
            {
                switch (chararray[i])
                {
                    case 'a':
                    case 'e':
                    case 'i':
                    case 'o':
                    case 'u':
                        chararray[i] -= (char)32;
                        break;
                    case ' ':
                        chararray[i] = '_';
                        break;
                }
                encriptedint[i] = chararray[i];
                
            }
            string textoencriptado = string.Join(" ", encriptedint);
            return textoencriptado;
        }
        static void Main(string[] args)
        {
            string mistring = "hola que tal estamos";
            Console.Write(encriptar(mistring));
            Console.ReadKey();
        }
    }
}