using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio4
{
    class Ejercicio4
    {

        public struct Point
        {
            public short x, y;
            public byte r, g, b;
            public float alpha;
        }

        public static float distance2Points(Point point1, Point point2)
        {
            float distance = (float)Math.Sqrt((float)Math.Pow((point2.x - point1.x), 2) + (float)Math.Pow((point2.y - point1.y), 2));
            return distance;
        }

        static void Main(string[] args)
        {
            Point point1, point2;

            Console.WriteLine("Inserta una coordenada x del primer punto: ");
            point1.x = short.Parse(Console.ReadLine());
            Console.WriteLine("Inserta una coordenada y del primer punto: ");
            point1.y = short.Parse(Console.ReadLine());
            Console.WriteLine("Inserta un valor (red) del primer punto(0-255): ");
            point1.r = byte.Parse(Console.ReadLine());
            Console.WriteLine("Inserta un valor (green) del primer punto(0-255): ");
            point1.g = byte.Parse(Console.ReadLine());
            Console.WriteLine("Inserta un valor (blue) del primer punto(0-255): ");
            point1.b = byte.Parse(Console.ReadLine());
            Console.WriteLine("Inserta un valor (alfa) del primer punto: ");
            point1.alpha = float.Parse(Console.ReadLine());


            Console.WriteLine("Inserta una coordenada x del segundo punto: ");
            point2.x = short.Parse(Console.ReadLine());
            Console.WriteLine("Inserta una coordenada y del segundo punto: ");
            point2.y = short.Parse(Console.ReadLine());
            Console.WriteLine("Inserta un valor (red) del segundo punto(0-255): ");
            point2.r = byte.Parse(Console.ReadLine());
            Console.WriteLine("Inserta un valor (green) del segundo punto(0-255): ");
            point2.g = byte.Parse(Console.ReadLine());
            Console.WriteLine("Inserta un valor (blue) del segundo punto(0-255): ");
            point2.b = byte.Parse(Console.ReadLine());
            Console.WriteLine("Inserta un valor (alfa) del segundo punto: ");
            point2.alpha = float.Parse(Console.ReadLine());

            Console.WriteLine("Coordenadas del punto1 = [{0},{1}] , ColorRGBA = [{2},{3},{4},{5}] ", point1.x, point1.y, point1.r, point1.g, point1.b, point1.alpha);
            Console.WriteLine("Coordenadas del punto2 = [{0},{1}] , ColorRGBA = [{2},{3},{4},{5}]  ", point2.x, point2.y, point2.r, point2.g, point2.b, point2.alpha);
            Console.WriteLine("Distancia entre los puntos = {0}", distance2Points(point1, point2));

            Console.ReadKey();
        }
    }
}
