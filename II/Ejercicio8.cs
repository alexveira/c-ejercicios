using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio8
{
    public static class Encriptator
    {
        public static string Encriptar(string texto)
        {
            char[] mychararray = texto.ToCharArray();
            for (int i = 0; i < mychararray.Length; i++)
            {
                mychararray[i] += (char)1;

            }
            string textoencriptado = new string(mychararray);
            return textoencriptado;
        }

        public static string Desencriptar(string texto)
        {
            char[] mychararray = texto.ToCharArray();
            for (int i = 0; i < mychararray.Length; i++)
            {
                mychararray[i] -= (char)1;

            }
            string textodesencriptado = new string(mychararray);
            return textodesencriptado;
        }
    }

    class Ejercicio8
    {
        static void Main(string[] args)
        {
            string texto = "hola";
            string textoencriptado = "ipmb";
            Console.WriteLine(Encriptator.Desencriptar(textoencriptado));
            Console.WriteLine(Encriptator.Encriptar(texto));
            Console.ReadKey();

        }
    }
}
