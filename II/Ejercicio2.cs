using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio2
{
    class Ejercicio2
    {
        static void minMaxArray(int[] array, ref int min, ref int max)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] < min)
                    min = array[i];
                if (array[i] > max)
                    max = array[i];
            }
        }

        static void Main(string[] args)
        {
            int[] array = { 2, 3, 1, 5 };
            int max = array[0];
            int min = array[0];
            minMaxArray(array, ref min, ref max);
            Console.WriteLine("El numero minimo del array es: {0} el maximo es: {1}", min, max);
            Console.ReadKey();
        }
    }
}
