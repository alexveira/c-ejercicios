using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static int[] getRamdomArray(int size, int minNUM, int maxNUM)
        {
            Random rnd = new Random();
            int[] array = new int[size];
            for (int i = 0; i < size; i++)
            {
                array[i] = rnd.Next(minNUM, maxNUM);
            }
            return array;
        }

        static void Main(string[] args)
        {
            int[] miArrayRandom = getRamdomArray(6, 2, 9);
            for (int n = 0; n < 6; n++)
            {
                Console.WriteLine("Posicion {0} del array => {1}", n, miArrayRandom[n]);
            }
            Console.ReadKey();
        }
    }
}
