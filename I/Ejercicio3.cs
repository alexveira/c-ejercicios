using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {

        static void Main(string[] args)
        {
            string line = Console.ReadLine();
            int grade = int.Parse(line);

            switch (grade)
            {
                case 10:
                case 9:
                    Console.WriteLine("Sobresaliente");
                    break;
                case 8:
                case 7:
                    Console.WriteLine("Notable");
                    break;
                case 6:
                case 5:
                    Console.WriteLine("Suficiente");
                    break;
                case 4:
                    Console.WriteLine("Suspenso");
                    break;
                default:
                    Console.WriteLine("Suspenso");
                    break;
            }
            Console.ReadKey();

        }
    }
}
