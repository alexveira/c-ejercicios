using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static int[] getRamdomArray(int size, int minNUM, int maxNUM)
        {
            Random rnd = new Random();
            int[] array = new int[size];
            for (int i = 0; i < size; i++)
            {
                array[i] = rnd.Next(minNUM, maxNUM);
            }
            return array;
        }
        
        static int[] invertArray(int[] array)
        {
            int[] copyarray = array;

            for (int i = 0; i < (copyarray.Length / 2); i++)
            {
                int temporary = copyarray[i];
                copyarray[i] = copyarray[(copyarray.Length - 1) - i];
                copyarray[(array.Length - 1) - i] = temporary;
            }
            return copyarray;
        }
     
        static void Main(string[] args)
        {
            int[] myarray = getRamdomArray(6, 0, 9);
            Console.Write("Array de entrada: ");
            for (int n = 0; n < myarray.Length; n++)
            {
                Console.Write("{0}", myarray[n]);
            }
            Console.WriteLine();
            Console.Write("Array de salida:  ");
            int[] myarrayinvert = invertArray(myarray);
            
            for (int n = 0; n < myarray.Length; n++)
            {
                Console.Write("{0}", myarrayinvert[n]);
            }
            Console.ReadKey();
        }
    }
}
