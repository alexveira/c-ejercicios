using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
       

        static void Main(string[] args)
        {
            Random rnd = new Random();
            
            int[] array = new int[6];
            for (int i = 0; i < 50; i++)
            {
                int randomNumber = rnd.Next(0, 6);
                array[randomNumber] += 1;
            }

            for (int n = 0; n < 6; n++){
                Console.WriteLine("El numero {0} se ha repetido {1} veces", (n + 1).ToString(), array[n]);
            }
            Console.ReadKey();
        }
    }
}
