using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static bool searchInArray(int[,] miArray, int element){
            for(int i = 0; i < miArray.GetLength(0); i++){
                for (int j = 0; j < miArray.GetLength(1); j++)
                {
                    if(miArray[i,j] == element)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        static void Main(string[] args)
        {
            int[,] matrix = new int[4, 4] { { 1, 2, 6, 3 }, { 3, 4, 6, 2 }, 
                                            { 5, 3, 9, 6 }, { 5, 0, 3, 8 } };
            
            Console.WriteLine(searchInArray(matrix, 7));
            Console.ReadKey();
        }
    }
}
