using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            int result = 0;
            for (int i = 2; i < 499; i++)
            {
                if (i % 2 == 0)
                {
                    result += i;
                }
            }
            Console.WriteLine("El resultado de la suma de pares hasta 499 es: {0}", result);
            result = 0;
            for (int i = 1; i < 499; i++)
            {
                if (i % 2 != 0)
                {
                    result += i;
                }
            }
            Console.WriteLine("El resultado de la suma de impares hasta 499 es: {0}", result);


            Console.ReadKey();
        }
    }
}
