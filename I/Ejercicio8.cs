using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static int[] getRamdomArray(int size, int minNUM, int maxNUM)
        {
            Random rnd = new Random();
            int[] array = new int[size];
            for (int i = 0; i < size; i++)

            {
                array[i] = rnd.Next(minNUM, maxNUM);
            }
            return array;
        }

        static int basicArrayCalculator(int[] miArray, char operacion)
        {
            int result = 0;
            switch (operacion)
            {
                case '+':
                    for (int i = 0; i < miArray.Length; i++)
                    {
                        result += miArray[i];
                    }
                    break;
                case '*':
                    result = 1;
                    for (int i = 0; i < miArray.Length; i++)
                    {
                        
                        result *= miArray[i];
                    }
                    break;
                case 
					result = miArray[0];
                    for (int i = 1; i < miArray.Length; i++)
                    {
                        result -= miArray[i];
                    }
                    break;

            }
            return result;
        }

        static void Main(string[] args)
        {
            int[] miArrayRandom = getRamdomArray(6, 0, 9);
            Console.Write("El array es: ");
            for (int n = 0; n < miArrayRandom.Length; n++)
            {
                Console.Write("{0}", miArrayRandom[n]);
            }
            Console.WriteLine();
            Console.WriteLine("Inserta una operacion a realizar (+ , -, *)");
            string line = Console.ReadLine();
            char operacion = char.Parse(line);
            int result = basicArrayCalculator(miArrayRandom, operacion);
            Console.WriteLine("El resultado de la {0} es {1}", operacion, result);
            Console.ReadKey();
        }
    }
}