using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
   
        static void Main(string[] args)
        {
            Console.Write("Inserta el numero de veces que quieres que se repita Fibonacci: ");
            string line = Console.ReadLine();
            int repeticion = int.Parse(line);

            int a = 0, b = 1;
            for (int i = 0; i < repeticion; i++)
            {
                int aux = a;
                a = b;
                b = aux + b;
                Console.WriteLine(a);
            }
            Console.ReadKey();
        }
    }
}
