using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {

        static void Main(string[] args)
        {
            Console.WriteLine("Insetra un año");
            string line = Console.ReadLine();
            int ano = int.Parse(line);

            if ((ano % 4 == 0) && ((ano % 100 != 0) || (ano % 400 == 0)))
            {
                Console.WriteLine("Es Bisiesto");
            }
            else
            {
                Console.WriteLine("No es Bisiesto");
            }

            Console.ReadKey();

        }
    }
}
